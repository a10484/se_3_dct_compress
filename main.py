import math
from texttable import Texttable
from PIL import Image
import numpy as np
from matplotlib import pyplot as plt


def calc(f_image, f_hat_image, channal):
    f = np.asarray(f_image.getchannel(channal))
    f_hat = np.asarray(f_hat_image.getchannel(channal))
    #  Red Channel
    f_hat_sub_f_channel = f_hat - f
    f_hat_sub_f_exponentiation_2_channel = f_hat_sub_f_channel ** 2
    sum_f_hat_sub_f_exponentiation_2_channel = np.sum(
        f_hat_sub_f_exponentiation_2_channel)
    # MSE
    MSE = sum_f_hat_sub_f_exponentiation_2_channel / \
        (f_image.size[0] * f_image.size[1])
    # Root mean square error RMS
    RMS = math.sqrt(MSE)
    # PSNR
    PSNR = 10 * math.log10(255 / MSE)

    e_x_y = f - f_hat

    same_image = f - f

    black_mask = f - [0]

    white_mask = f - [255]

    return [[channal, RMS, MSE, PSNR], {channal: e_x_y}, {channal: f}, {channal: f_hat}, {channal: same_image}, {channal: black_mask}, {channal: white_mask}]


f_path = './images/f.jpg'
f_hat_path = './images/f_hat.jpg'

f = Image.open(f_path)
f_hat = Image.open(f_hat_path)

print()
channels = ['R', 'G', 'B']
images_info = [['Channel', 'RMS', "MSE", 'PSNR']]
error_f_f_hat = []
real_image = []
noise_image = []
same_image = []
black_mask = []
white_mask = []

for channel in channels:
    calc_channel = calc(f, f_hat, channel)
    images_info.append(calc_channel[0])
    error_f_f_hat.append(calc_channel[1][channel])
    real_image.append(calc_channel[2][channel])
    noise_image.append(calc_channel[3][channel])
    same_image.append(calc_channel[4][channel])
    black_mask.append(calc_channel[5][channel])
    white_mask.append(calc_channel[6][channel])

info_table = Texttable()
# table.set_deco(Texttable.VLINES)
info_table.set_cols_align(["c", "c", "c", "c"])
info_table.set_cols_valign(["m", "m", "m", "m"])
info_table.add_rows(images_info)
print(info_table.draw())
print()

# print(error_f_f_hat[0])
# image_error_f_f_hat = [
#     [error_f_f_hat[0][index], error_f_f_hat[1]
#      [index], error_f_f_hat[2][index]] for index, value in error_f_f_hat[0]
# ]
# print()
# print(image_error_f_f_hat)


def plot_image(image, name):
    rgb_image = np.dstack(
        (image[0], image[1], image[2]))

    im = Image.fromarray(rgb_image, 'RGB')

    plt.rcParams["figure.figsize"] = [7.00, 3.50]
    plt.rcParams["figure.autolayout"] = True
    plt.subplot(2, 2, 1)
    plt.title("R Channel")
    plt.imshow(image[0], cmap="Blues_r")
    plt.subplot(2, 2, 2)
    plt.title("G Channel")
    plt.imshow(image[1], cmap="Accent_r")
    plt.subplot(2, 2, 3)
    plt.title("B Channel")
    plt.imshow(image[2], cmap="terrain_r")
    plt.subplot(2, 2, 4)
    plt.title("RGB Channel")
    plt.imshow(im, cmap="twilight_shifted_r")
    plt.savefig(name + '.jpg')
    plt.show()


plot_image(error_f_f_hat, "error_f_f_hat")
plot_image(real_image, "real_image")
plot_image(noise_image, "noise_image")
plot_image(same_image, "same_image")
plot_image(black_mask, "black_mask")
plot_image(white_mask, "white_mask")